$(document).ready(function () {
    let swiper = new Swiper('.swiper-container', {
        loop: true,
        speed: 400,
        autoHeight: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        }
    })
    $('.curtain__btn a').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
    });
    $('.curtain__btn').on('click', function () {
        $(this).next().slideToggle(300);
    });
});